<!-- resources/views/about.blade.php -->
<!DOCTYPE html>
<html>
<head>
    <title>About Us</title>
    <!-- Tambahkan CSS yang diperlukan -->
    <link rel="stylesheet" type="text/css" href="{{asset('landing-pages/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('landing-pages/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('landing-pages/css/responsive.css')}}">
</head>
<body>
    <!-- Konten halaman About -->
    <div class="about_section layout_padding">
        <div class="container">
            <div class="about_section_2">
                <div class="row">
                    <div class="col-md-6">
                        <div class="about_taital_box">
                            <h1 class="about_taital">About Our Shop</h1>
                            <h1 class="about_taital_1">Coffee Distribution</h1>
                            <p class="about_text">Konten halaman about...</p>
                            <div class="readmore_btn"><a href="#">Read More</a></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="image_iman"><img src="{{asset('landing-pages/images/about-img.png')}}" class="about_img"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Tambahkan JS yang diperlukan -->
    <script src="{{asset('landing-pages/js/jquery.min.js')}}"></script>
    <script src="{{asset('landing-pages/js/popper.min.js')}}"></script>
    <script src="{{asset('landing-pages/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('landing-pages/js/jquery-3.0.0.min.js')}}"></script>
    <script src="{{asset('landing-pages/js/plugin.js')}}"></script>
</body>
</html>

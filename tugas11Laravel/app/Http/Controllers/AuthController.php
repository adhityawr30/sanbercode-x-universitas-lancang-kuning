<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function welcome() {
        return view('pages.welcome');
    }

    public function store(Request $request) {
        $firstName = $request->input('first_name');
        $lastName = $request->input('last_name');
        $gender = $request->input('gender');
        $nationality = $request->input('nationality');
        $languageSpoken = $request->input('language_spoken');
        $bio = $request->input('bio');

        return view('pages.welcome', [
            'first_name' => $firstName,
            'last_name' => $lastName,
            'gender' => $gender,
            'nationality' => $nationality,
            'language_spoken' => $languageSpoken,
            'bio' => $bio,
        ]);
    }
}
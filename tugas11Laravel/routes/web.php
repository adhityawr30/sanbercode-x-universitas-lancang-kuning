<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\FilmController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [HomeController::class, 'index']);

Route::get('/register', [AuthController::class, 'welcome']);

Route::post('/welcome', [AuthController::class, 'store']);

Route::get('/table', function(){
    return view('pages.table');
}); 

Route::get('/data-table', function(){
    return view('pages.data-table');
}); 

Route::resource('cast', CastController::class);

Route::resource('film', FilmController::class);
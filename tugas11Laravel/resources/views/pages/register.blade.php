<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>

    <form action="/welcome" method="POST">
        @csrf
        <label for="first_name">First Name:</label> <br> <br>
        <input type="text" name="first_name" id="first_name"> <br> <br>
        <label for="last_name">Last Name:</label> <br> <br>
        <input type="text" name="last_name" id="last_name"> <br> <br>
        <label for="gender">Gender:</label> <br> <br>
        <input type="radio" name="gender" value="Male"> Male <br>
        <input type="radio" name="gender" value="Female"> Female <br>
        <input type="radio" name="gender" value="Other"> Other <br> <br>
        <label for="nationality">Nationality:</label> <br> <br>
        <select name="nationality" id="nationality">
            <option value="Indonesia">Indonesia</option>
            <option value="Malaysia">Malaysia</option>
            <option value="Bekasi">Bekasi</option>
        </select> <br> <br>
        <label for="language_spoken">Language Spoken:</label> <br> <br>
        <input type="checkbox" name="language_spoken[]" value="Bahasa Indonesia"> Bahasa Indonesia <br>
        <input type="checkbox" name="language_spoken[]" value="English"> English <br> 
        <input type="checkbox" name="language_spoken[]" value="Other"> Other <br> <br>
        <label for="bio">Bio:</label> <br> <br>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea> <br> <br>
        <button type="submit">Sign Up</button>
    </form> 
</body>
</html>
@extends('layouts.master')

@section('title')
    Halaman Tambah Film
@endsection

@section('content')

        <img src="{{asset('image/' . $film->poster)}}" class="card-img-top" alt="...">
            <div class="card-body">
                 <p class="card-text">{{$film->content}} </p>
                <a href="/film" class="btn btn-secondary btn-block btn-sm">Kembali</a>
            </div>

@endsection

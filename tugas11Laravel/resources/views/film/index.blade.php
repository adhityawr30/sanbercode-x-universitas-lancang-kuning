@extends('layouts.master')

@section('title')
    Halaman Tambah Film
@endsection

@section('content')

<a href="/film/create" class="btn btn-primary btn-sm m-b4">Tambah Film</a>

<div class="row">
    @forelse ($film as $item)
        <div class="col-4">
            <div class="card">
                <img src="{{asset('image/' . $item->poster)}}" class="card-img-top" alt="...">
                <div class="card-body">
                    <h3>{{$item->judul}}</h3>
                    <p class="card-text">{{ Str::limit($item->ringkasan, 20) }}</p>
                    <a href="/post/{{$item->id}}" class="btn btn-secondary btn-block btn-sm">Read Me</a>
                </div>
            </div>
        </div>
    @empty
        <h2>Tidak Ada Film</h2>
    @endforelse
</div>

@endsection

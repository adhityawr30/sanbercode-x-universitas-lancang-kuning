@extends('layouts.master')

@section('title')
    Halaman Tambah Film
@endsection

@section('content')
<form action="/film" method="post" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label for="judul">Judul</label>
        <input type="text" class="form-control" name="judul" id="judul">
        @error('judul')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="ringkasan">Ringkasan</label>
        <textarea class="form-control" name="ringkasan" id="ringkasan"></textarea>
        @error('ringkasan')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="tahun">Tahun</label>
        <input type="number" class="form-control" name="tahun" id="tahun">
        @error('tahun')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="poster">Poster</label>
        <input type="file" class="form-control" name="poster" id="poster">
        @error('poster')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
    <label>Genre</label>
    <select name="genre_id" class="form-control" id="">
        <option value="">--pilih genre--</option>
        @forelse ($genres as $genre)
            <option value="{{ $genre->id }}">{{ $genre->nama }}</option>
        @empty
            <option value="">Tidak ada genre tersedia</option>
        @endforelse
    </select>
    @error('genre_id')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>

    <button type="submit" class="btn btn-primary">Tambah</button>
</form>
@endsection
